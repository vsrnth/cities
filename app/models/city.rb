# frozen_string_literal: true

require 'open-uri'
require 'yaml'
# City Model
class City < ApplicationRecord
  def self.fetch_data
    html_data = File.read("#{Rails.root}/data.html")
    unparsed_data = Nokogiri::HTML.parse(html_data)
    puts unparsed_data
    parse_data(unparsed_data)
  end

  def self.parse_data(unparsed_data)
    columns = City.column_names - %w[id created_at updated_at]
    data_array = []

    unparsed_data.at('table').search('tr').each do |row|
      cells = row.search('td').map { |cell| cell.text.strip }
      data_array << columns.zip(cells[0].gsub(', ', '  ').split('  ')<< cells[1] << cells[2]).to_h unless cells[0].nil?
    end
    insert_data(data_array)
  end

  def self.insert_data(data)
    City.create(data)
  end
end
