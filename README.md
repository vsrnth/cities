# README

- Run `City.delete_all` to clear previously present data.
- Run `City.fetch_data` in `rails console` to add data from data.html into the db after parsing.
- The code has been modified to accomodate for latitude and longitude
- You can run through the git commits to look at the non lattitude and longitude version of the code.