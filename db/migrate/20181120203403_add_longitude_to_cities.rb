class AddLongitudeToCities < ActiveRecord::Migration[5.2]
  def change
    add_column :cities, :longitude, :decimal
  end
end
